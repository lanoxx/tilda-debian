# About

This repository contains packaging files for the `tilda` package in Debian. The Tilda source
repository can be found on Github under https://github.com/lanoxx/tilda/

For instructions about packaging see https://github.com/lanoxx/tilda/blob/master/HACKING.md
